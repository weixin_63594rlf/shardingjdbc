# 介绍

1. 使用前点击 docker-compose.yml 中的绿色按钮进行启动
2. IDEA 启动失败可参考 https://blog.csdn.net/weixin_63594548/article/details/140962044
   或在 docker-compose.yml 文件所属目录执行 `docker-compose up`

# 依赖

1.33 是能够让项目运行的最高版本

```xml

<dependency>
    <groupId>org.yaml</groupId>
    <artifactId>snakeyaml</artifactId>
    <version>1.33</version>
</dependency>
```

以下两个依赖必须引入

```xml

<dependency>
    <groupId>javax.xml.bind</groupId>
    <artifactId>jaxb-api</artifactId>
    <version>2.3.1</version>
</dependency>
```

```xml

<dependency>
    <groupId>org.glassfish.jaxb</groupId>
    <artifactId>jaxb-runtime</artifactId>
    <version>2.3.1</version>
</dependency>
```

XA 事务可引入下列依赖

```xml

<dependency>
    <groupId>io.shardingsphere</groupId>
    <artifactId>sharding-transaction-spring-boot-starter</artifactId>
    <version>3.1.0</version>
</dependency>
```

# 本地事务

不涉及 RPC 的事务

# XA 事务

涉及 RPC 的事物，如 RMI 和 RestTemplate

```text
// 开启 XA 事物，需要和 @Transactional 一起使用
@ShardingTransactionType(TransactionType.XA)
```

