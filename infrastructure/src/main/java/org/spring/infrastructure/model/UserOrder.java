package org.spring.infrastructure.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
@ToString
public class UserOrder {
    /**
    * 自增 ID；【必须保留自增 ID，不要将一些有随机特性的字段值设计为主键，
     * 例如 order_id，会导致 innodb 内部 page分裂和大量随机 I/O，性能下降】
     * int 大约 21 亿左右，超过会报错。bigint 大约 9 千亿左右。
    */
    private Long id;

    /**
    * 用户姓名；
    */
    private String userName;

    /**
    * 用户编号；
    */
    private String userId;

    /**
    * 用户电话；使用 varchar(20) 存储手机号，不要使用整型。
     * 手机号不会做数学计算、涉及到区号或者国家代号，可能出现+-()、
     * 支持模糊查询，例如：like“135%”
    */
    private String userMobile;

    /**
    * 商品编号
    */
    private String sku;

    /**
    * 商品名称
    */
    private String skuName;

    /**
    * 订单ID
    */
    private String orderId;

    /**
    * 商品数量；整形定义中不显示规定显示长度，比如使用 INT，而不使用 INT(4)
    */
    private Integer quantity;

    /**
    * 商品价格；小数类型为 decimal，禁止使用 float、double
    */
    private BigDecimal unitPrice;

    /**
    * 折扣金额；
    */
    private BigDecimal discountAmount;

    /**
    * 费率金额；
    */
    private BigDecimal tax;

    /**
    * 支付金额；(商品的总金额 - 折扣) * (1 - 费率)
    */
    private BigDecimal totalAmount;

    /**
    * 订单日期；timestamp的时间范围在
     * 1970-01-01 00:00:01到2038-01-01 00:00:00之间
    */
    private Date orderDate;

    /**
    * 订单状态；0 创建、1完成、2掉单、3关单
     * 【不要使用 enum 要使用 tinyint 替代。0-80 范围，
     * 都可以使用 tinyint】
    */
    private Integer orderStatus;

    /**
    * 逻辑删单；0未删除，1已删除 【表达是否概念的字段必须使用is_】
    */
    private Boolean isDelete;

    /**
    * 唯一索引；分布式下全局唯一，用于binlog 同步 ES 方便使用
    */
    private String uuid;

    /**
    * 设备地址；存储 IPV4地址，通过 MySQL 函数转换，
     * inet_ntoa、inet_aton 示例；SELECT INET_ATON
     * (‘209.207.224.40′); 3520061480 SELECT INET_
     * NTOA(3520061480); 209.207.224.40所有字段定义为NOT NULL，
     * 并设置默认值，因为null值的字段会导致每一行都占用额外存储空间\n数据迁移容易出错，在聚合函数计算结果偏差（如count结果不准）并且null的列使索引/索引统计/值比较都更加复杂，MySQL内部需要进行特殊处理，表中有较多空字段的时候，数据库性能下降严重。开发中null只能采用is null或is not null检索，而不能采用=、in、<、<>、!=、not in这些操作符号。如：where name!=’abc’，如果存在name为null值的记录，查询结果就不会包含name为null值的记录
    */
    private String ipv4;

    /**
    * 设备地址；存储 IPV6 地址，VARBINARY(16)  插入：INET6_ATON('2001:0db8:85a3:0000:0000:8a2e:0370:7334') 查询：SELECT INET6_NTOA(ip_address)
    */
    private byte[] ipv6;

    /**
    * 扩展数据；记录下单时用户的设备环境等信息(核心业务字段，要单独拆表)。【select user_name, ext_data, ext_data->>'$.device', ext_data->>'$.device.machine' from `user_order`;】
    */
    private String extData;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 创建时间
    */
    private Date createTime;

}