package org.spring.infrastructure.mapper;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.shardingsphere.driver.jdbc.core.datasource.ShardingSphereDataSource;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.spring.domain.user.service.entity.UserOrderEntity;
import org.spring.infrastructure.model.UserOrder;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.UUID;

@SpringBootTest
public class UserOrderMapperTest {
    @Autowired
    UserOrderMapper userOrderMapper;

//    @Test
    @RepeatedTest(2)
    public void selectByPrimaryKeyTest() {
        System.out.println(userOrderMapper.selectByPrimaryKey(1L));
    }

    @Test
//    @RepeatedTest(2)
    void insertTest() {

        UserOrderEntity userOrderEntity = UserOrderEntity.builder()
                .userName("用户名")
                .userId(RandomStringUtils.randomAlphabetic(6))
                .userMobile("+86 13521408***")
                .sku("13811216")
                .skuName("《手写MyBatis：渐进式源码实践》")
                .orderId(RandomStringUtils.randomNumeric(11))
                .quantity(1)
                .unitPrice(BigDecimal.valueOf(128))
                .discountAmount(BigDecimal.valueOf(50))
                .tax(BigDecimal.ZERO)
                .totalAmount(BigDecimal.valueOf(78))
                .orderDate(new Date())
                .orderStatus(0)
                .isDelete(false)
                .uuid(UUID.randomUUID().toString().replace("-", ""))
                .ipv4("127.0.0.1")
                .ipv6("2001:0db8:85a3:0000:0000:8a2e:0370:7334".getBytes())
                .extData("{\"device\": {\"machine\": \"IPhone 14 Pro\", \"location\": \"shanghai\"}}")
                .build();

        UserOrder userOrder = new UserOrder();
        BeanUtils.copyProperties(userOrderEntity, userOrder);
        System.out.println(userOrderMapper.insert(userOrder));


    }

    @Test
    public void test() {

    }

}
