package org.spring.domain.user.repository;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.spring.domain.user.service.entity.UserOrderEntity;
import org.spring.infrastructure.model.UserOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@SpringBootTest
public class UserOrderRepositoryTest {
    @Autowired
    private UserOrderRepository userOrderRepository;

    @Test
    @Transactional
    void saveTest() {
        UserOrderEntity userOrderEntity = UserOrderEntity.builder()
                .userName("用户名")
                .userId(RandomStringUtils.randomAlphabetic(6))
                .userMobile("+86 123456789***")
                .sku("123456789")
                .skuName("《手写MyBatis：渐进式源码实践》")
                .orderId(RandomStringUtils.randomNumeric(11))
                .quantity(1)
                .unitPrice(BigDecimal.valueOf(128))
                .discountAmount(BigDecimal.valueOf(50))
                .tax(BigDecimal.ZERO)
                .totalAmount(BigDecimal.valueOf(78))
                .orderDate(new Date())
                .orderStatus(0)
                .isDelete(false)
                .uuid(UUID.randomUUID().toString().replace("-", ""))
                .ipv4("127.0.0.1")
                .ipv6("2001:0db8:85a3:0000:0000:8a2e:0370:7334".getBytes())
                .extData("{\"device\": {\"machine\": \"IPhone 14 Pro\", \"location\": \"shanghai\"}}")
                .build();
        UserOrder save = userOrderRepository.save(userOrderEntity);
        System.out.println(save);
    }
}

