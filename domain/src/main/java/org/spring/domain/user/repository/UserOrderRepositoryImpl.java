package org.spring.domain.user.repository;

import lombok.RequiredArgsConstructor;
import org.spring.domain.user.service.entity.UserOrderEntity;
import org.spring.infrastructure.mapper.UserOrderMapper;
import org.spring.infrastructure.model.UserOrder;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@RequiredArgsConstructor
public class UserOrderRepositoryImpl implements UserOrderRepository {
    final UserOrderMapper userOrderMapper;

    @Override
    @Transactional
    public UserOrder save(UserOrderEntity userOrderEntity) {
        UserOrder userOrder = new UserOrder();
        BeanUtils.copyProperties(userOrderEntity, userOrder);
        userOrderMapper.insert(userOrder);
        return userOrder;
    }
}
