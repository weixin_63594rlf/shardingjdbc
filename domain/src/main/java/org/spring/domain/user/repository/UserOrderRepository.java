package org.spring.domain.user.repository;

import org.spring.domain.user.service.entity.UserOrderEntity;
import org.spring.infrastructure.model.UserOrder;

public interface UserOrderRepository {
    UserOrder save(UserOrderEntity userOrderEntity);
}
