package org.spring.domain.user.service.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@Builder
public class UserOrderEntity {
    private Long id;

    private String userName;

    private String userId;

    private String userMobile;

    private String sku;

    private String skuName;

    private String orderId;

    private Integer quantity;

    private BigDecimal unitPrice;

    private BigDecimal discountAmount;

    private BigDecimal tax;

    private BigDecimal totalAmount;

    private Date orderDate;

    private Integer orderStatus;

    private Boolean isDelete;

    private String uuid;

    private String ipv4;

    private byte[] ipv6;

    private String extData;

    private Date updateTime;

    private Date createTime;
}
